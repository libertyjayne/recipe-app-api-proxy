# recipe-app-api-proxy

NGIX proxy app for our recipe app API

## Usage

### Environment Variables

* `LISTEN_PORT` - Port to listen on (default: `8000`)
* `APP_HOST` - Hostname of the app to forward requests to (default: `app`)
* `APP_PORT` - Port of the app to forward requests to (default: `9000`)

#### Notes
nginxinc/nginx-unprivileged 
- use this instead of more popular because popular version uses root user

Alpine 
- most light weight version of NGINX image
- Linux OS designed for Docker containers
- No unnecessary packages, allows you to add things specific for your app

Linux permissions
- https://chmod-calculator.com/